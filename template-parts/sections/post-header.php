<?php 
/*----------------------------------------------------------------*\

	POST HEADER
	Display the post title

\*----------------------------------------------------------------*/
?>

<?php if ( get_field('header_style') == 'action' ) : ?>
	<header class="action-header">
		<h1><?php the_title(); ?></h1>
		<?php if ( get_field('header_description') ) : ?>
			<p class="subheader"><?php the_field('header_description') ?></p>
		<?php endif; ?>
		<?php
			if ( get_field('header_button') ) :
			$link = get_field('header_button'); 
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self'; 
		?>
		<a class="button is-paint" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
			<?php echo esc_html($link_title); ?>
		</a>
		<?php endif; ?>
		<?php if ( get_field('header_image') ) : ?>
			<?php $image = get_field('header_image'); ?>
			<svg viewBox="0 0 1440 320">
				<use xlink:href="#wave"></use>
			</svg>
			<img class="lazyload blur-up" data-expand="100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['xlarge']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 500w, <?php echo $image['sizes']['large']; ?> 700w, <?php echo $image['sizes']['xlarge']; ?> 1000w"  alt="<?php echo $image['alt']; ?>">
		<?php endif ?>
	</header>
<?php else : ?>
	<?php $columns = get_field('header_columns'); ?>
	<header class="post-head columns-<?php echo $columns; ?>">
		<h1>
			<?php the_title(); ?>
		</h1>
		<?php if ( get_field('header_image') ) : ?>
			<?php $image = get_field('header_image'); ?>
			<img class="lazyload blur-up" data-expand="100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['xlarge']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 500w, <?php echo $image['sizes']['large']; ?> 700w, <?php echo $image['sizes']['xlarge']; ?> 1000w"  alt="<?php echo $image['alt']; ?>">
		<?php endif ?>
		<?php if ( get_field('header_image_two') && get_field('header_columns') > 1 ) : ?>
			<?php $image = get_field('header_image_two'); ?>
			<img class="lazyload blur-up" data-expand="100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['xlarge']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 500w, <?php echo $image['sizes']['large']; ?> 700w, <?php echo $image['sizes']['xlarge']; ?> 1000w"  alt="<?php echo $image['alt']; ?>">
		<?php endif ?>
		<?php if ( get_field('header_image_three') && get_field('header_columns') > 2 ) : ?>
			<?php $image = get_field('header_image_three'); ?>
			<img class="lazyload blur-up" data-expand="100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['xlarge']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 350w, <?php echo $image['sizes']['medium']; ?> 500w, <?php echo $image['sizes']['large']; ?> 700w, <?php echo $image['sizes']['xlarge']; ?> 1000w"  alt="<?php echo $image['alt']; ?>">
		<?php endif ?>
	</header>
<?php endif; ?>