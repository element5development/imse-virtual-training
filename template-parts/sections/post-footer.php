<?php 
/*----------------------------------------------------------------*\

	POST FOOTER
	Display copyright and navigation

\*----------------------------------------------------------------*/
?>

<footer class="post-footer">
	<div>
		<div>
			<div>
				<a href="https://imse.com/">
					<img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo-imse.svg" alt="institute for multi-sensory education" />
				</a>
			</div>
			<div>
				<span class="title">Follow Us</span>
				<nav>
					<ul class="social-nav">
						<li>
							<a target="_blank" href="https://www.facebook.com/Orton-Gillingham-358905134128313/">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-fb.svg" alt="institute for multi-sensory education" />
							</a>
						</li>
						<li>
							<a target="_blank" href="https://www.pinterest.com/ortongillingham/">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-pinterest.svg" alt="institute for multi-sensory education" />
							</a>
						</li>
						<li>
							<a target="_blank" href="https://www.youtube.com/channel/UCPJqkhSNKynPzgLj4Xuqp9g?view_as=subscriber">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-youtube.svg" alt="institute for multi-sensory education" />
							</a>
						</li>
						<li>
							<a target="_blank" href="https://twitter.com/IMSEOG">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-twitter.svg" alt="institute for multi-sensory education" />
							</a>
						</li>
						<li>
							<a target="_blank" href="https://www.linkedin.com/company/orton-gillingham/">
								<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-linked.svg" alt="institute for multi-sensory education" />
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</footer>
<footer class="copyright">
	<div>
		<div>
			<p>© <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All Rights Reserved.</p>
			<ul>
				<li>
					<a href="https://imse.com/privacy-policy/">Privacy Policy</a>
				</li>
				<li>
					<a href="https://s3.amazonaws.com/imse-production/cms_page_media/2017/12/20/IMSE+Terms+and+Conditions.pdf">Terms & Conditions</a>
				</li>
			</ul>
		</div>
		<a target="_blank" href="https://element5digital.com" rel="nofollow">
			<img src="https://element5digital.com/wp-content/themes/e5-starting-point/dist/images/element5_credit_alt.svg" alt="Crafted by Element5 Digital" />
		</a>
	</div>
</footer>