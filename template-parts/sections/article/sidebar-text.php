<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	containing a two column system in which one column contains card grid

\*----------------------------------------------------------------*/
?>

<div class="text_sidebar <?php the_sub_field('width'); ?>">
	<section class="editor">
		<?php the_sub_field('content-right'); ?>
	</section>
	<section class="card-grid text-cards columns-1">
		<?php while ( have_rows('sidebar-cards') ) : the_row(); ?>
			<div class="card">
				<!-- HEADLINE -->
				<?php if ( get_sub_field('title') ) : ?>
					<h2><?php the_sub_field('title') ?></h2>
				<?php endif; ?>
				<!-- DESCRIPTION -->
				<div class="description">
					<?php if ( get_sub_field('description') ) : ?>
						<?php the_sub_field('description'); ?>
					<?php endif; ?>
				</div>	
				<!-- BUTTON -->
				<div>
					<?php
						if ( get_sub_field('button_one') ) : 
						$link = get_sub_field('button_one'); 
						$link_url = $link['url'];
						$link_title = $link['title'];
						$link_target = $link['target'] ? $link['target'] : '_self'; 
					?>
						<a class="button <?php if ($format == 'image-top') : ?>is-red<?php endif ?>" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
							<?php echo esc_html($link_title); ?>
						</a>
					<?php endif; ?>
					<!-- BUTTON -->
					<?php
						if ( get_sub_field('button_two') ) : 
						$link = get_sub_field('button_two'); 
						$link_url_two = $link['url'];
						$link_title_two = $link['title'];
						$link_target_two = $link['target'] ? $link['target'] : '_self'; 
					?>
						<a class="button is-text" href="<?php echo esc_url($link_url_two); ?>" target="<?php echo esc_attr($link_target_two); ?>">
							<?php echo esc_html($link_title_two); ?> >
						</a>
					<?php endif; ?>
				</div>
			</div>
		<?php endwhile; ?>
	</section>
</div>