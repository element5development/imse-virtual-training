<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying lastest testimony is a slider format

\*----------------------------------------------------------------*/
?>
<?php $posts = get_sub_field('testimonies'); ?>

<?php if( $posts ): ?>
	<section class="testimonies <?php the_sub_field('width'); ?>">
		<h2>What other teachers are saying about IMSE training</h2>
		<ul>
			<?php $i=0; foreach( $posts as $post): $i++; // variable must be called $post (IMPORTANT) ?>
			<li id="testimony-<?php echo $i; ?>" class="testimony">
				<blockquote>
					<p><span>“</span><?php the_field('quote'); ?></p>
				</blockquote>
			</li>
			<?php endforeach; ?>
		</ul>
	</section>
	<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; ?>