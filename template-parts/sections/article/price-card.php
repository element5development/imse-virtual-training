<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	display a price card

\*----------------------------------------------------------------*/
?>
<section class="price-card is-extra-wide">
	<?php if ( get_sub_field('image') ) : ?>
		<?php $image = get_sub_field('image'); ?>
		<img class="lazyload blur-up" data-expand="100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['xlarge']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 250w, <?php echo $image['sizes']['medium']; ?> 350w, <?php echo $image['sizes']['large']; ?> 500w, <?php echo $image['sizes']['xlarge']; ?> 800w"  alt="<?php echo $image['alt']; ?>">
	<?php endif ?>
	<div class="description">
		<?php if ( get_sub_field('content') ) : ?>
			<?php the_sub_field('content'); ?>
		<?php endif; ?>
	</div>	
	<div class="price">
		<p><?php the_sub_field('price') ?></p>
		<?php
			if ( get_sub_field('button') ) : 
			$link = get_sub_field('button'); 
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self'; 
		?>
			<a class="button" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
				<?php echo esc_html($link_title); ?>
			</a>
		<?php endif; ?>
	</div>
</section>