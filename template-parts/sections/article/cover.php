<?php 
/*----------------------------------------------------------------*\

	ARTICLE SECTION
	displaying content with background image

\*----------------------------------------------------------------*/
?>

<?php if ( get_sub_field('background_image') ) : ?>
	<?php $background = get_sub_field('background_image'); ?>
	<section class="cover <?php the_sub_field('width'); ?>" style="background-image: url(<?php echo $background['sizes']['xlarge']; ?>);">
		<div>
			<div>
				<?php the_sub_field('content'); ?>
					<?php
					$link = get_sub_field('button'); 
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self'; 
					if ( get_sub_field('button') ) : 
				?>
					<a class="button is-paint" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
						<?php echo esc_html($link_title); ?>
					</a>
				<?php endif; ?>
			</div>
		</div>
	</section>
<?php else : ?>
	<section class="cover <?php the_sub_field('width'); ?>">
		<div>
			<?php the_sub_field('content'); ?>
		</div>
		<?php
			$link = get_sub_field('button'); 
			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self'; 
			if ( get_sub_field('button') ) : 
		?>
			<a class="button is-paint" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>">
				<?php echo esc_html($link_title); ?>
			</a>
		<?php endif; ?>
	</section>
<?php endif; ?>
