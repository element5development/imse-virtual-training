<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION

\*----------------------------------------------------------------*/
?>
<nav class="simplfied-nav">
	<a href="https://imse.com/">
		<img class="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/logo-imse-bg.svg" alt="institute for multi-sensory education" />
	</a>
	<a href="https://imse.com/training/?course=&state=Virtual&city=" class="button">
		<?php if ( wp_is_mobile() ) : ?>
			Virtual Trainings
		<?php else : ?>
			Register For Virtual Training
		<?php endif; ?>
	</a>
</nav>