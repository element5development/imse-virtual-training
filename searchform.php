<?php 
/*----------------------------------------------------------------*\

	SEARCH FORM
	This form is most commonly referanced in the navigation.php file.

\*----------------------------------------------------------------*/
?>

<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
  <label for="s" class="screen-reader-text">Search</label>
  <input type="search" id="s" name="s" placeholder="<?php echo esc_attr_x( 'Search …', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>" />
  <button type="submit">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-search.svg" alt="search site" />
	</button>
</form>