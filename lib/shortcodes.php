<?php
/*----------------------------------------------------------------*\
	INITIALIZE BUTTON SHORTCODE
\*----------------------------------------------------------------*/
function button_shortcode($atts, $content = null) {
	$atts = shortcode_atts(
		array(
			'size' => 'normal',
			'color' => 'black',
			'target' => '_self',
			'url' => '#',
		),
		$atts,
		'button'
	);
	$size = $atts['size'];
	$color = $atts['color'];
	$target = $atts['target'];
	$url = $atts['url'];

	$link = '<a target="'.$target.'" href="'.$url.'" class="button is-'.$color.' is-'.$size.'">' . do_shortcode($content) . '</a>';
  return $link;
}
add_shortcode( 'button', 'button_shortcode' );
/*----------------------------------------------------------------*\
	INITIALIZE SUBHEADER SHORTCODE
\*----------------------------------------------------------------*/
function subheader_shortcode($atts, $content = null) {
	$atts = shortcode_atts(
		array(
			'class' => 'normal',
		),
		$atts,
		'subheader'
	);
	$class = $atts['class'];

	$subheader = '<span class="subheader is-'.$class.'">' . do_shortcode($content) . '</span>';
  return $subheader;
}
add_shortcode( 'subheader', 'subheader_shortcode' );
/*----------------------------------------------------------------*\
	INITIALIZE DETAILS SHORTCODE
\*----------------------------------------------------------------*/
function details_shortcode($atts, $content = null) {
	$atts = shortcode_atts(
		array(
			'class' => 'normal',
			'title' => 'title'
		),
		$atts,
		'details'
	);
	$class = $atts['class'];
	$title = $atts['title'];

	$details = '<details><summary>'.$title.'</summary><div>' . do_shortcode($content) . '</div></details>';
  return $details;
}
add_shortcode( 'details', 'details_shortcode' );
/*----------------------------------------------------------------*\
	INITIALIZE ANIMATED BOX SHORTCODE
\*----------------------------------------------------------------*/
function animated_boxes_shortcode($atts, $content = null) {
	$atts = shortcode_atts(
		array(
			'class' => 'normal',
		),
		$atts,
		'animated-box'
	);
	$class = $atts['class'];

	$animated_boxes = '<ul class="animated-boxes">' . do_shortcode($content) . '</ul>';
  return $animated_boxes;
}
add_shortcode( 'animated-boxes', 'animated_boxes_shortcode' );
function animated_box_shortcode($atts, $content = null) {
	$atts = shortcode_atts(
		array(
			'class' => 'normal',
			'title' => 'title'
		),
		$atts,
		'animated-box'
	);
	$class = $atts['class'];
	$title = $atts['title'];

	$animated_box = '<li class="animated-box">'.$title.'<div class="content">' . do_shortcode($content) . '</div></li>';
  return $animated_box;
}
add_shortcode( 'animated-box', 'animated_box_shortcode' );
/*----------------------------------------------------------------*\
		INITIALIZE BLOCKQUOTE QUOTEE SHORTCODE
\*----------------------------------------------------------------*/
function quotee_shortcode($atts, $content = null) {
	$atts = shortcode_atts(
		array(
			'class' => 'normal',
			'name' => 'name'
		),
		$atts,
		'quotee'
	);
	$class = $atts['class'];
	$name = $atts['name'];

	$quotee = '<footer class="quotee"><span class="name">'.$name.'</span>' . do_shortcode($content) . '</footer>';
  return $quotee;
}
add_shortcode( 'quotee', 'quotee_shortcode' );