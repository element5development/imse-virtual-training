<?php
/*----------------------------------------------------------------*\
	ADDITIONAL IMAGE SIZES
\*----------------------------------------------------------------*/
add_image_size( 'placeholder', 20, 20 ); 
function placeholder_image_sizes($sizes) {
	$sizes['placeholder'] = __( 'Placeholder' );
	return $sizes;
}
add_filter('image_size_names_choose', 'placeholder_image_sizes');
add_image_size( 'small', 400, 400 ); 
function small_image_sizes($sizes) {
	$sizes['small'] = __( 'Small' );
	return $sizes;
}
add_filter('image_size_names_choose', 'small_image_sizes');
add_image_size( 'xlarge', 2000, 2000 ); 
function large_image_sizes($sizes) {
	$sizes['xlarge'] = __( 'X-Large' );
	return $sizes;
}
add_filter('image_size_names_choose', 'large_image_sizes');

/*----------------------------------------------------------------*\
    ADDITIONAL FILE FORMATS
\*----------------------------------------------------------------*/
function my_myme_types($mime_types){
	$mime_types['svg'] = 'image/svg+xml'; //Adding svg extension
	$mime_types['psd'] = 'image/vnd.adobe.photoshop'; //Adding photoshop files
	return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);