<?php 
/*----------------------------------------------------------------*\

	HTML FOOTER CONTENT
	Commonly only used to close off open containers
	External resources are not referanced here, refer to the functions.php

\*----------------------------------------------------------------*/
?>

<?php wp_footer(); ?>

</body>

</html>