<?php 
/*----------------------------------------------------------------*\

	Template Name: Featured Product Archive
	
\*----------------------------------------------------------------*/
?>
<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

get_template_part('template-parts/elements/navigation');

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
<header class="woocommerce-products-header">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h2>Products</h2>
	<?php endif; ?>

	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>
</header>
<?php
if ( woocommerce_product_loop() ) {

	?><div class="page-title-header"><?php
	if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title"><?php the_title(); ?></h1>
	<?php endif; 

	/**
	 * Hook: woocommerce_before_shop_loop.
	 *
	 * @hooked woocommerce_output_all_notices - 10
	 * @hooked woocommerce_result_count - 20
	 * @hooked woocommerce_catalog_ordering - 30
	 */
	do_action( 'woocommerce_before_shop_loop' );

	?></div><?php

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			/**
			 * Hook: woocommerce_shop_loop.
			 */
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	$args = array(
		'post_type' => 'product',
		'posts_per_page' => 12,
		'tax_query' => array(
			array(
				'taxonomy' => 'product_visibility',
				'field'    => 'name',
				'terms'    => 'featured',
			),
		),
	);
	$loop = new WP_Query( $args );
	if ( $loop->have_posts() ) {
			while ( $loop->have_posts() ) : $loop->the_post();
					wc_get_template_part( 'content', 'product' );
			endwhile;
	} else {
			echo __( 'No products found' );
	}
	wp_reset_postdata();

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );

?><div class="article-content"><?php
	/*----------------------------------------------------------------*\
	|
	| Insert page content which is most often handled via ACF Pro
	| and highly recommend the use of the flexiable content so
	|	we already placed that code here.
	|
	| https://www.advancedcustomfields.com/resources/flexible-content/
	|
	\*----------------------------------------------------------------*/
	while ( have_rows('article') ) : the_row();
		if( get_row_layout() == 'editor' ):
			get_template_part('template-parts/sections/article/editor');
		elseif( get_row_layout() == '2editor' ):
			get_template_part('template-parts/sections/article/editor-2-column');
		elseif( get_row_layout() == '3editor' ):
			get_template_part('template-parts/sections/article/editor-3-column');
		elseif( get_row_layout() == 'media+text' ):
			get_template_part('template-parts/sections/article/media-text');
		elseif( get_row_layout() == 'sidebar+text' ):
			get_template_part('template-parts/sections/article/sidebar-text');
		elseif( get_row_layout() == 'cover' ):
			get_template_part('template-parts/sections/article/cover');
		elseif( get_row_layout() == 'gallery' ):
			get_template_part('template-parts/sections/article/gallery');
		elseif( get_row_layout() == 'card_grid' ):
			get_template_part('template-parts/sections/article/card-grid');
		elseif( get_row_layout() == 'testimonies' ):
			get_template_part('template-parts/sections/article/testimonies');
		elseif( get_row_layout() == 'price_card' ):
			get_template_part('template-parts/sections/article/price-card');
		endif;
	endwhile;
?></div><?php

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

get_template_part('template-parts/sections/post-footer');

get_footer( 'shop' );