<?php 
/*----------------------------------------------------------------*\

	Template Name: Training Single 
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/notification-bar'); ?>

<?php get_template_part('template-parts/elements/cookie-bar'); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php
	$courseid = get_query_var('cid');
	$val=array();
	$result = file_get_contents('https://shop.imse.com/api/session/'.$courseid);
	//echo $result;
	// Will dump a beauty json :3
	$courseData = json_decode($result, true);
	$sno=1;

	foreach ($courseData as $key => $value) {
		$val=$value;
	}		
	if(isset($_GET['calendar']) && ($_GET['calendar']=='Apple' || $_GET['calendar']=='Outlook')) {
		// nothing
	}
?>

<header class="register-card">
	<section class="price-card is-extra-wide">
		<?php if ( get_field('training_header_image') ) : ?>
			<?php $image = get_field('training_header_image'); ?>
			<img class="lazyload blur-up" data-expand="100" data-sizes="auto" src="<?php echo $image['sizes']['placeholder']; ?>" data-src="<?php echo $image['sizes']['xlarge']; ?>" data-srcset="<?php echo $image['sizes']['small']; ?> 250w, <?php echo $image['sizes']['medium']; ?> 350w, <?php echo $image['sizes']['large']; ?> 500w, <?php echo $image['sizes']['xlarge']; ?> 800w"  alt="<?php echo $image['alt']; ?>">
		<?php endif ?>
		<div class="description">
			<h1>
				<?php echo $val['title']; ?>
			</h1>
			<div class="details">
				<div>
					<h2>Availability</h2>
					<p>
						<?php  
							if($val['available_seats']>0){
								echo 'Available';
							}else{
								echo 'Waitlist';
							}
						?>
					</p>
				</div>
				<div>
					<h2>Training Dates</h2>
					<p>
						<?php
							$sdate=date_create($value['start_date']);
							$edate=date_create($value['end_date']);
							echo date_format($sdate, 'M d') .' - '. date_format($edate, 'M d, Y');
						?>
					</p>
					<div class="open-btn">
						<button class="open-button button is-text" onclick="openForm()">
							<strong>Add to Calendar</strong>
						</button>
					</div>
					<div id="loginPopup">
      			<div class="form-popup" id="popupForm">
							<ul>
								<li><a href="http://www.google.com/calendar/event?action=TEMPLATE&text=<?php echo $val['title']; ?>&dates=<?php echo date_format($sdate, 'Ymd'); ?>/<?php echo date_format($edate, 'Ymd'); ?>&details=<?php echo $val['title']; ?>&location=<?php echo $val['street'] .','.$val['city'].','.$val['state']; // pulled in from django ?>" target="_blank"><img src="https://imsesite.wpengine.com/wp-content/uploads/2020/04/icon-google-t5.svg">Google </a></li>
								<li><a href="http://calendar.live.com/calendar/calendar.aspx?rru=addevent&summary=<?php echo $val['title']; ?>&dtstart=<?php echo date_format($sdate, 'Ymd'); ?>&dtend=<?php echo date_format($edate, 'Ymd'); ?>&description=<?php echo $val['title']; ?>&location=<?php echo $val['street'] .','.$val['city'].','.$val['state']; // pulled in from django ?>" target="_blank"><img src="https://imsesite.wpengine.com/wp-content/uploads/2020/04/icon-outlookcom-t5.svg">Outlook</a></li>
								<li><a href="http://calendar.yahoo.com/?v=60&title=<?php echo $val['title']; ?>&st=<?php echo date_format($sdate, 'Ymd'); ?>&et=<?php echo date_format($edate, 'Ymd'); ?>&desc=<?php echo $val['title']; ?>&in_loc=<?php echo $val['street'] .','.$val['city'].','.$val['state']; // pulled in from django ?>" target="_blank"><img src="https://imsesite.wpengine.com/wp-content/uploads/2020/04/icon-yahoo-t5.svg">Yahoo </a></li>
								<li><a class="test-ics"><img src="https://imsesite.wpengine.com/wp-content/uploads/2020/04/icon-apple-t5.svg">Apple</a></li>
								<li><a class="test-ics" ><img src="https://imsesite.wpengine.com/wp-content/uploads/2020/04/icon-outlookcom-t5.svg">Outlook (Offline)</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div>
					<h2>Location</h2>
					<p>
						<?php if ( !empty($val['street']) ) : ?>
							<?php echo $val['street'] .', ' ?>
						<?php endif; ?>
						<?php echo $val['city'].', '.$val['state']; ?>
					</p>
					<a class="button is-text" href="https://www.google.com/maps/dir/?api=1&amp;destination=<?php echo $val['latitude']; ?>,<?php echo $val['longitude']; ?>" target="_blank">
						Directions >
					</a>
				</div>
			</div>
		</div>	
		<div class="price">
			<p>$<?php echo $val['price']; ?></p>
			<?php if($val['available_seats']>0) { ?>
				<a class="button is-text" href="#register">
					Register for this course
				</a>
				<?php }else{ ?>
					<a class="button is-text" href="https://admin.imse.com/login/?redirect=/training/<?php echo $_GET['cid']; ?>/">
						Join the waitlist
					</a>
				<?php } ?>
		</div>
	</section>
</header>

<main id="main-content">
	<article>
		<section class="editor standard">
			<h2>Description</h2>
			<?php echo $val['description']; ?>
		</section>
		<section id="register" class="registration editor standard">
			<?php if($value['available_seats'] > 0){ ?>
				<h2>Register For This Course</h2>
				<!-- https://imse.com/login/?training=<?php echo $_GET['cid']; ?>&quantity=1&for_self=on -->
				<form id="sForm" action="https://admin.imse.com/login/" method="GET">
					<input type="hidden" name="training" id="training" value="<?php echo $courseid; ?>">
					<label>Registering For</label>
					<ul class="gfield_radio">
						<li>
							<input type="radio" id="self" name="for_self" value="on">
							<label for="self">Myself</label>
						</li>
						<li>
							<input type="radio" id="someone" name="for_self" value="off">
							<label for="someone">Someone Else</label>
						</li>
						<li>
							<input type="radio" id="multiple" name="for_self" value="off">
							<label for="multiple">Multiple People</label>
						</li>
					</ul>
					<label for="cars">Number Of Participants</label>
					<select id="quantity" name="quantity">
						<option value="1">1</option>
						<option value="2">2</option>
						<option value="3">3</option>
						<option value="4">4</option>
					</select>
					<button type="submit" value="Register">Register</button>
				</form>
				<br/>
				<p><strong>Online registration options include:</strong><br/>
				Pay by credit card will confirm your registration.<br/>
				Pay by check/purchase order options are available, and will hold your seat to allow time for processing. Registrations are not confirmed until the payment or purchase order is received.</p>
			<?php } else { ?>
				<a href="https://admin.imse.com/login/?redirect=/training/<?php echo $courseid; ?>" class="button">Join The Waitlist</a>
			<?php } ?>
		</section>
		<section class="editor standard">
			<h3>Cancellation Policy:</h3>
			<p>If you cancel prior to the start date of your training, you will be charged a $50 cancellation fee. If you cancel 14 days or less prior to the start date of your training, you will be charged a $100 fee.</p>
			<p>There is no fee for transferring your registration to another training if you transfer before 14 days prior to a training. If you transfer your training 14 days or less prior to the start date of your training, you will be charged a $100 fee.</p>
			<p>If a participant needs to make up more than one day of training, there will be a $100 charge.</p>
			<p>The Institute for Multi-Sensory Education reserves the right to cancel. If IMSE must cancel, 100% of the training fee will be refunded.</p>
		</section>
		<?php 
		/*----------------------------------------------------------------*\
		|
		| Insert page content which is most often handled via ACF Pro
		| and highly recommend the use of the flexiable content so
		|	we already placed that code here.
		|
		| https://www.advancedcustomfields.com/resources/flexible-content/
		|
		\*----------------------------------------------------------------*/
		?>
		<?php
			while ( have_rows('article') ) : the_row();
				if( get_row_layout() == 'editor' ):
					get_template_part('template-parts/sections/article/editor');
				elseif( get_row_layout() == '2editor' ):
					get_template_part('template-parts/sections/article/editor-2-column');
				elseif( get_row_layout() == '3editor' ):
					get_template_part('template-parts/sections/article/editor-3-column');
				elseif( get_row_layout() == 'media+text' ):
					get_template_part('template-parts/sections/article/media-text');
				elseif( get_row_layout() == 'sidebar+text' ):
					get_template_part('template-parts/sections/article/sidebar-text');
				elseif( get_row_layout() == 'cover' ):
					get_template_part('template-parts/sections/article/cover');
				elseif( get_row_layout() == 'gallery' ):
					get_template_part('template-parts/sections/article/gallery');
				elseif( get_row_layout() == 'card_grid' ):
					get_template_part('template-parts/sections/article/card-grid');
				elseif( get_row_layout() == 'testimonies' ):
					get_template_part('template-parts/sections/article/testimonies');
				elseif( get_row_layout() == 'price_card' ):
					get_template_part('template-parts/sections/article/price-card');
				endif;
			endwhile;
		?>
	</article>
	<aside>
		<div class="credits card">
			<h2>Course Credits</h2>
			<p>State Continuing Education Clock Hours (SCECH). Units provided through Schoolcraft College.</p>
			<p><b>Comprehensive Training:</b></p>
			<p class="hours"><?php echo $val['Hours']; ?> hours<br/><?php echo $val['Hours']; ?> SCECHs </p>
			<p>Disclaimer: We still need copy for this discalimer here for out-of-state attendees.</p>
		</div>
		<div class="share card">
			<?php the_field('send_this_page'); ?>
		</div>
	</aside>
</main>

<?php get_template_part('template-parts/sections/post-footer'); ?>

<?php get_footer(); ?>

<script>
	function fnRegisterNow(){
		document.getElementById("sForm").submit();
	}
	function fnRegisterNow1(){
		document.getElementById("for_self").value="off";
		fnRegisterNow();
	}
	function fnJoinWaitlistNow(){
		var t = document.getElementById("training").value;
		window.location.href = "https://admin.imse.com/login/?redirect=training/"+t+"/";
	}
</script>
<script>
	var todayDate	= new Date();
	var msgData	= todayDate.toISOString();
	<?php
		$sdat = date_create($value['start_date']);
		$edate=date_create($value['end_date']);
		$sdate=date_format($sdate, 'Y,m,d');
		$edate=date_format($edate, 'Y,m,d');
	?>
	var sdate = new Date(<?php echo $sdate; ?>);
	var edate = new Date(<?php echo $edate; ?>);
	var startDate	= sdate.toISOString();
	var endDate	= edate.toISOString();
	var icsMSG1 = "BEGIN:VCALENDAR\r\nVERSION:2.0\r\nPRODID:https://www.google.com/\r\nBEGIN:VEVENT\r\nUID:https://www.google.com/\r\nDTSTAMP:" + msgData + "Z\r\nDTSTART:" + startDate + "\r\n";
	var icsMSG2 = '';
	if(endDate != '') {
		icsMSG2 = "DTEND:" + endDate +"\r\n";
	}
	var title="<?php echo $val['title'] ?>";
	icsMSG3 = "SUMMARY:" + title + "\r\nEND:VEVENT\r\nEND:VCALENDAR";
	icsMSG = icsMSG1 + icsMSG2 + icsMSG3;
	$('.test-ics').click(function(){
			window.open( "data:text/calendar;charset=utf8," + escape(icsMSG));
	});
</script>